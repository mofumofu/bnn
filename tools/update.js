const crawl = require('./crawler');
const parse = require('./parser');
const { resolve } = require('path');
const fs = require('fs');

/* eslint-disable no-console */

// config
const delay = 1000; // delay between BNN requests
const result = [];
const pagePath = resolve(__dirname, '../public');

const write = (name, data) => {
  fs.writeFileSync(resolve(pagePath, `${name}.json`), JSON.stringify(data));
};

function writeResults() {
  const stats = {
    updated: new Date().toLocaleDateString(),
    total: result.length,
    novels: result.reduce((r, t) => (r += ~t.title.toLowerCase().indexOf('novel') ? 1 : 0), 0),
  };

  write('stats', stats);
  write('data', result);

  console.log('Done!');
}

function run(max, pageCount = 1) {
  crawl(pageCount)
  .then(
    pages => {
      result.push(...parse(pages));
      console.log(`Parsed page number ${pageCount}, currently holding ${result.length} products`);

      if (max && pageCount === max) {
        writeResults();
      } else {
        setTimeout(() => run(max, pageCount + 1), delay);
      }
    },
    done => {
      if (done === null) {
        writeResults();
      } else if (done.statusCode === 502) {
        console.log('error 502, waiting before for retry...');
        setTimeout(() => (max, pageCount), 5000);
      } else {
        console.log(done);
      }
    }
  );
}

fs.stat(pagePath, err => {
  if (err) {
    fs.mkdirSync(pagePath);
  }
  console.log('Working...');
  run(...process.argv.slice(2).map(e => Number(e) || undefined));
});

const request = require('request');

/* eslint-disable no-console, prefer-arrow-callback */
const amazonResultsPerPage = 12;
const jars = { amazon: request.jar(), bnn: request.jar() }; // to have consistent cookies, I guess
const header = { headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36' } };

function requestPage(target, jar) {
  console.log(target);
  return new Promise((resolve, reject) =>
    request(target, Object.assign(header, { jar }), function getPage(err, res, body) {
      if (err || res.statusCode !== 200) {
        reject(err || res);
      } else {
        resolve(body);
      }
    })
  );
}

function get(page) {
  const target = typeof page === 'number'
    ? `https://www.barnesandnoble.com/b/manga-mania-sale-buy-2-get-the-3rd-free/books/_/N-1gn4Z8q8?Nrpp=40&Ns=P_Display_Name%7C0&page=${page}`
    : new Array(Math.ceil(page.length / amazonResultsPerPage)).fill(null).map((e, i) =>
      `https://www.amazon.com/gp/search/ref=sr_adv_b/?search-alias=stripbooks&field-isbn=${page.join('|')}&page=${i + 1}`
    );

  return typeof page === 'number'
    ? requestPage(target, jars.bnn)
    : Promise.all(target.map(e => requestPage(e, jars.amazon)));
}

function parseBnnProducts(page) {
  const container = ~page.indexOf('digitalData1') ? 'digitalData1' : 'digitalData'; // ???
  let result = page.slice(page.indexOf(`var ${container}`) - 1);
  result = result.slice(0, result.indexOf('};') + 2);
  eval(result.replace(`var ${container}`, 'result')); // eslint-disable-line no-eval
  return result.product;
}

function crawler(bnnPage) {
  const data = {};

  return get(bnnPage)
  .then(result => {
    if (~result.indexOf('We found 0 results for')) {
      return Promise.reject(null);
    }
    data.body = result;
    data.products = parseBnnProducts(result);
    data.amazon = null;
    return data;
    // return get(data.products.map(e => e.productInfo.sku));
  })
  // .then(amazonPages => {
  //   data.amazon = amazonPages;
  //   return data;
  // }, err => { console.log(`ERROR: ${err.body}`)});

  // amazon is being a bitch about crawling, fuck it then
}

module.exports = crawler;

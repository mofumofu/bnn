const cheerio = require('cheerio');

/* eslint-disable no-console, no-prototype-builtins, prefer-arrow-callback */

function convertISBN(isbn) {
  let isbnnum = String(isbn).replace(/[-\s]/g, '').toUpperCase();
  let total = 0;
  let z = isbnnum.charAt(12);
  isbnnum = isbnnum.substring(3, 12);
  total = 0;

  for (let x = 0; x < 9; x++) {
    total += (isbnnum.charAt(x) * (10 - x));
  }
  z = (11 - (total % 11)) % 11;
  z = z === 10 ? 'X' : z;
  return `${isbnnum}${z}`;
}

function parseBnnData(body, products) {
  const bnnSelector = cheerio.load(body);
  const books = bnnSelector('.product-shelf-tile-book');

  const pageResult = products.reduce((r, t, i) =>
    Object.assign(r, { [t.productInfo.sku]: {
      title: t.productInfo.productName,
      price: t.price.basePrice,
      author: bnnSelector('div.product-shelf-author.pt-0 > a', books[i]).first().text(),
      isbn: t.productInfo.sku,
    } }), {}
  );

  return pageResult;
}

function parseAmazonData(body, bnnData) {
  const amazonSelector = cheerio.load(body);
  const books = amazonSelector('[data-asin]');
  const amazonMap = Object.keys(bnnData).reduce((r, t) =>
    Object.assign(r, { [convertISBN(bnnData[t].isbn)]: bnnData[t] }), {});

  books.each(function parseAmazonBook(i, b) {
    const book = amazonSelector(b);
    const id = book.attr('data-asin');

    if (amazonMap[id]) {
      const priceBlock = amazonSelector('[aria-label]', book).first();
      const bookData = amazonMap[id];

      if (priceBlock.length) {
        bookData.amazonPrice = priceBlock.attr('aria-label').replace('$', '');
        bookData.amazonFormat = priceBlock.parent().parent().prev().text();
      } else {
        bookData.amazonFormat = 'N/A';
      }
    }
  });
}

function parser({ body, products, amazon}) {
  const parsedBNN = parseBnnData(body, products);
  
  if (amazon) {
    amazon.forEach(e => parseAmazonData(e, parsedBNN));
  }

  return Object.keys(parsedBNN).map(e => parsedBNN[e]);
}

module.exports = parser;
